package ar.com.nicolasbianchi.mealdbapp.model

import retrofit2.http.GET
import retrofit2.http.Query

interface MealDBApiInterface {

    @GET("search.php")
    suspend fun searchMeals(@Query("s") query: String): Meals

    @GET("lookup.php")
    suspend fun getMealDetails(@Query("i") idMeal: String): Meals
}