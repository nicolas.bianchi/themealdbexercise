package ar.com.nicolasbianchi.mealdbapp.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import javax.inject.Inject

class MealSearchViewModelFactory @Inject constructor(private val mealSearchViewModel: MealSearchViewModel): ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(MealSearchViewModel::class.java!!)) {
            return mealSearchViewModel as T
        }
        throw IllegalArgumentException("Unknown class name")
    }
}