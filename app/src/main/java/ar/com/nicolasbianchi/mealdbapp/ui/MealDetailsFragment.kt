package ar.com.nicolasbianchi.mealdbapp.ui

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider

import ar.com.nicolasbianchi.mealdbapp.R
import ar.com.nicolasbianchi.mealdbapp.model.Meal
import ar.com.nicolasbianchi.mealdbapp.viewmodel.MealDetailsViewModel
import ar.com.nicolasbianchi.mealdbapp.viewmodel.MealDetailsViewModelFactory
import ar.com.nicolasbianchi.mealdbapp.viewmodel.MealSearchViewModel
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject


private const val ARG_PARAM_MEAL_ID = "meal_id"

private const val TAG = "MealDetailsFragment"

class MealDetailsFragment : Fragment() {

    @Inject
    lateinit var mealDetailsViewModelFactory: MealDetailsViewModelFactory
    private val mealDetailsViewModel: MealDetailsViewModel by lazy {
        ViewModelProvider(this, mealDetailsViewModelFactory).get(
            MealDetailsViewModel::class.java
        )
    }

    private var mealId: String = ""


    private lateinit var titleTextView: TextView
    private lateinit var instrucitonsTextView: TextView

    companion object {
        @JvmStatic
        fun newInstance(idMeal: String) =
            MealDetailsFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM_MEAL_ID, idMeal)
                }
            }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mealId = arguments?.getString(ARG_PARAM_MEAL_ID) ?: ""


    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        AndroidSupportInjection.inject(this)
        val view = inflater.inflate(R.layout.fragment_meal_details, container, false)
        titleTextView = view.findViewById(R.id.meal_details_title)
        instrucitonsTextView = view.findViewById(R.id.meal_details_instructions)

        return view
    }

    override fun onStart() {
        super.onStart()
        getMealDetails(mealId)
    }


    fun getMealDetails(idMeal: String) {
        mealDetailsViewModel.mealDetailsResults(idMeal).observe(
            viewLifecycleOwner,
            Observer { meal ->
                meal?.let {
                    Log.d(TAG, "got meal details ui: ${meal.strMeal}")
                    updateUI(meal)
                }
            })

    }

    private fun updateUI(meal: Meal) {
        titleTextView.text = meal.strMeal
        instrucitonsTextView.text = meal.strInstructions

    }


}
