package ar.com.nicolasbianchi.mealdbapp.ui


import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.SearchView
import androidx.cardview.widget.CardView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import ar.com.nicolasbianchi.mealdbapp.R
import ar.com.nicolasbianchi.mealdbapp.model.Meal
import ar.com.nicolasbianchi.mealdbapp.viewmodel.MealSearchViewModel
import ar.com.nicolasbianchi.mealdbapp.viewmodel.MealSearchViewModelFactory
import com.squareup.picasso.Picasso
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject


private const val TAG = "MealSearchFragment"

class MealSearchFragment : Fragment() {

    @Inject
    lateinit var mealSearchViewModelFactory: MealSearchViewModelFactory
    private val mealSearchViewModel: MealSearchViewModel by lazy {
        ViewModelProvider(this, mealSearchViewModelFactory).get(
            MealSearchViewModel::class.java)
    }



    private lateinit var mealsRecyclerView: RecyclerView
    private var adapter: MealAdapter? = MealAdapter(emptyList())

    private var listener: OnMealSearchInteractionListener? = null

    interface OnMealSearchInteractionListener {
        fun onMealTouched(meal: Meal)
    }



    companion object {
        @JvmStatic
        fun newInstance() = MealSearchFragment()
    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnMealSearchInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnMealSearchInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)

    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_meal_search, menu)

        val searchItem: MenuItem = menu.findItem(R.id.menu_item_search)
        val searchView = searchItem.actionView as SearchView
        searchView.apply {
            setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(queryText: String): Boolean {
                    Log.d(TAG, "QueryTextSubmit: $queryText")
                    searchMeals(queryText)
                    return true
                }

                override fun onQueryTextChange(queryText: String): Boolean {
                    Log.d(TAG, "QueryTextChange: $queryText")
                    return false
                }
            })
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        AndroidSupportInjection.inject(this)
        val view = inflater.inflate(R.layout.fragment_meal_search, container, false)

        mealsRecyclerView = view.findViewById(R.id.meals_recycler_view) as RecyclerView
        mealsRecyclerView.layoutManager = LinearLayoutManager(context)
        mealsRecyclerView.adapter = adapter

        return view
    }

    fun searchMeals(query: String) {
        mealSearchViewModel.mealResults(query).observe(
            viewLifecycleOwner,
            Observer { meals ->
                meals?.let {
                    Log.d(TAG, "got meals ui: ${meals.size}")
                    updateUI(meals)
                }
            })

    }

    private fun updateUI(meals: List<Meal>) {
        adapter = MealAdapter(meals)
        mealsRecyclerView.adapter = adapter
    }

    private inner class MealAdapter(var meals: List<Meal>) : RecyclerView.Adapter<MealHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MealHolder {
            val view = layoutInflater.inflate(R.layout.meal_list_item, parent, false)
            return MealHolder(view)
        }

        override fun getItemCount(): Int {
            return   meals.size
        }

        override fun onBindViewHolder(holder: MealHolder, position: Int) {
            val meal = meals[position]
            holder.bind(meal)
        }

    }
    private inner class MealHolder(view: View): RecyclerView.ViewHolder(view) {
        private lateinit var meal: Meal
        private val cardViewContainer: CardView = itemView.findViewById(R.id.meal_card)
        private val mealTitleTextView: TextView = itemView.findViewById(R.id.meal_title)
        private val mealCategoryTextView: TextView = itemView.findViewById(R.id.meal_category)
        private val mealImageTextView: ImageView = itemView.findViewById(R.id.meal_image)


        fun bind(meal: Meal) {
            this.meal = meal
            Picasso.get().load(meal.strMealThumb)
                .resize(192,192)
                .into(mealImageTextView)
            mealTitleTextView.text = meal.strMeal
            mealCategoryTextView.text = meal.strCategory

            cardViewContainer.setOnClickListener { Log.d(TAG, "card touched!")
            listener?.onMealTouched(meal)}
        }

    }



}
