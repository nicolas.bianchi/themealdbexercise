package ar.com.nicolasbianchi.mealdbapp.model

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject

private const val TAG = "MealDBRepository"

class MealDBRepository @Inject constructor(val api: MealDBApiInterface) {

    fun searchMeals(query: String): LiveData<List<Meal>> {
        return liveData(Dispatchers.IO) {
            try {
                val meals = searchMealsFromApi(query)
                emit(meals)
            } catch (e: Exception) {
                Log.e(TAG, "error getting meals from api: ${e.message}")
            }

        }

    }

    fun getMealDetails(idMeal: String): LiveData<Meal> {
        return liveData(Dispatchers.IO) {
            try {
                val meals = getMealDetailsFromApi(idMeal)
                if (meals.isNotEmpty())
                {
                    emit(meals.first())
                }

            } catch (e: Exception) {
                Log.e(TAG, "error getting meals from api: ${e.message}")
            }

        }

    }

    private suspend fun searchMealsFromApi(query: String): List<Meal> {
        var list = emptyList<Meal>()
        try {
            val mealsResponse = api.searchMeals(query)
            list = mealsResponse.meals
            Log.d(TAG, "GOT MEALS ${list.size}")

        } catch (e: Exception) {
            Log.e(TAG,"ERROR FETCHING MEALS: ${e.message}")
        }
        return list
    }

    private suspend fun getMealDetailsFromApi(idMeal: String): List<Meal> {
        var list = emptyList<Meal>()
        try {
            val mealsResponse = api.getMealDetails(idMeal)
            list = mealsResponse.meals
            Log.d(TAG, "GOT MEAL DETAILS ${list.size}")

        } catch (e: Exception) {
            Log.e(TAG,"ERROR FETCHING MEAL DETAILS: ${e.message}")
        }
        return list
    }

}