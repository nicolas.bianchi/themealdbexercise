package ar.com.nicolasbianchi.mealdbapp.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import ar.com.nicolasbianchi.mealdbapp.model.Meal
import ar.com.nicolasbianchi.mealdbapp.model.MealDBRepository
import javax.inject.Inject

class MealDetailsViewModel @Inject constructor(private val mealDBRepository: MealDBRepository): ViewModel() {

    fun mealDetailsResults(idMeal: String): LiveData<Meal> = mealDBRepository.getMealDetails(idMeal)
}