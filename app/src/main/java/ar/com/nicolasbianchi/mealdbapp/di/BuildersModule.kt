package ar.com.nicolasbianchi.mealdbapp.di

import ar.com.nicolasbianchi.mealdbapp.MainActivity
import ar.com.nicolasbianchi.mealdbapp.ui.MealDetailsFragment
import ar.com.nicolasbianchi.mealdbapp.ui.MealSearchFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class BuildersModule {

    @ContributesAndroidInjector
    abstract fun contributeMainActivity(): MainActivity

    @ContributesAndroidInjector
    abstract fun contributeMealSearchFragment(): MealSearchFragment

    @ContributesAndroidInjector
    abstract fun contributeMealDetailsFragment(): MealDetailsFragment
}