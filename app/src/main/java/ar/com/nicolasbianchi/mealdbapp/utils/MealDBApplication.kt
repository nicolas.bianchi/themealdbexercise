package ar.com.nicolasbianchi.mealdbapp.utils

import android.app.Application
import ar.com.nicolasbianchi.mealdbapp.BuildConfig
import ar.com.nicolasbianchi.mealdbapp.di.AppModule
import ar.com.nicolasbianchi.mealdbapp.di.DaggerAppComponent
import ar.com.nicolasbianchi.mealdbapp.di.NetModule
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import javax.inject.Inject

class MealDBApplication: Application(), HasAndroidInjector {

    @Inject
    lateinit var androidInjector: DispatchingAndroidInjector<Any>

    override fun onCreate() {
        super.onCreate()
        DaggerAppComponent.builder()
            .netModule(NetModule(BuildConfig.API_URL))
            .appModule(
                AppModule(this)

            ).build().inject(this)
    }

    override fun androidInjector(): AndroidInjector<Any> = androidInjector
}