package ar.com.nicolasbianchi.mealdbapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.fragment.app.Fragment
import ar.com.nicolasbianchi.mealdbapp.model.Meal
import ar.com.nicolasbianchi.mealdbapp.ui.MealDetailsFragment
import ar.com.nicolasbianchi.mealdbapp.ui.MealSearchFragment

const val TAG = "MealSearchFragment"
class MainActivity : AppCompatActivity(),  MealSearchFragment.OnMealSearchInteractionListener {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

       val currentFragment = supportFragmentManager.findFragmentById(R.id.fragment_container)
        if (currentFragment == null) {
            val fragment = MealSearchFragment.newInstance()
            supportFragmentManager.beginTransaction().add(R.id.fragment_container, fragment)
                .commit()
        }
    }

    override fun onMealTouched(meal: Meal) {
        val fragment = MealDetailsFragment.newInstance(meal.idMeal)
        supportFragmentManager.beginTransaction().replace(R.id.fragment_container, fragment)
            .commit()
    }
}
