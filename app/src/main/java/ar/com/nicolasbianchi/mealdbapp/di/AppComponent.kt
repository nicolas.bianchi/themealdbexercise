package ar.com.nicolasbianchi.mealdbapp.di

import ar.com.nicolasbianchi.mealdbapp.utils.MealDBApplication
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(AndroidInjectionModule::class, BuildersModule::class, AppModule::class, NetModule::class))
interface AppComponent {
    fun inject(app: MealDBApplication)
}