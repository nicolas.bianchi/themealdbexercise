package ar.com.nicolasbianchi.mealdbapp.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import javax.inject.Inject

class MealDetailsViewModelFactory @Inject constructor(private val mealDetailsViewModel: MealDetailsViewModel): ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(MealDetailsViewModel::class.java!!)) {
            return mealDetailsViewModel as T
        }
        throw IllegalArgumentException("Unknown class name")
    }
}